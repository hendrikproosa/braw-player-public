# BRAW format player for Win

*Version 0.6.0 (200827)*

*Written by Hendrik Proosa*

License is: do whatever you want with it, if it breaks you get to keep the pieces


Blackmagic and third-party licences are in installation dir
Qt libraries are linked dynamically, Qt itself is licensed under LGPL license
See more information on qt.io

Current version is built against BRAW SDK version 2.0, meaning it should be able to decode newer files also. Currently only CPU decode works, but player automatically changes decoding resolution behind the scenes if viewer size is smaller, so to get faster playback just resize the window. It is also possible to manually set decode resolution using combobox in UI, smaller resolution of the two is used.

### WHATS NEW in v0.6.0 (released 200827)
Support for Braw SDK v2.0 beta 2, so supports braw files from the newest cameras and sources, including new 12K Ursa Mini Pro.

Timeline zoom, to better see specific frame ranges and mark in-out points. Zoom functionality lays ground for zoomed range playback and other stuff.

### WHATS NEW in v0.5.0 (released 200715)
Support for Braw SDK v1.8, so supports braw files from the newest cameras and sources.

Command line arguments! Launch player from command line with specified file and IO points or trim new braw file straight from command. See COMMAND LINE section below for information.

Overhaul of decoding engine, including caching mechanism (still in progress though), braw trimming and other improvements. No GPU decode still. Audio engine currently disabled, I will work on it again soon.

Metadata viewer now allows copying values, in case one wants to save them as text.

Keymap got some modifications, see keymap help (press H in viewer).

### WHATS NEW in v0.3.0 (released 190704)
Rudimentary audio playback. There are buffering issues which must be sorted (audio is choppy and cracking) but speech is audible and thus somewhat usable. Audio plays during scrubbing too and conforms to framerate changes (plays slower or faster).
Removed notification about BMD Custom and development settings, it messed up the layout.


### WHATS NEW in v0.2.4 (released 190628)
Shaders can now access up to four additional image textures for additional overlays, effects etc. Images must be in shaders/ folder and named tex1.png, tex2.png and so on. They must be png format! See example shaders and images.

For using texture in shader, access it through standard texture uniforms, but with index: uniform sampler2D tex1;

Accessible shader uniforms list added to end of this document.


### WHATS NEW in v0.2.3 (released 190627)
Modified fragment shader examples code and added a few new ones (distortion, blanking).


### WHATS NEW in v0.2.2 (released 190626)
Fragment shaders are now loaded from shaders/ dir directly, meaning that any shader file with .frag ending will be loaded and is selectable in LUT combobox.


### WHATS NEW in v0.2.1 (released 190626)
Added application icon for eye candy purposes.


### WHATS NEW in v0.2 (released 190625)
Fixed direct .braw file opening bug, now player can be set as default application for loading .braw files.
Frame level metadata now available.

Fragment and vertex shaders fully exposed for viewer OpenGL widget, see shaders/ subdir.
Removed some cuda probing code that might trigger missing cuda lib error with AMD cards (not tested if works though).

Braw bug prevented opening files directly through os by setting player as default application. Fixed the loading and also problems with stylesheet and icons. It should now work as expected.

Frame level metadata exposes some stuff not visible on clip level, mainly frame-processing attributes and also some lens and cam information (aperture, internal_nd, distance, shutter_value etc).

Regarding shaders, as viewer surface is drawn with opengl, user can now use fragment shader to do any image processing one wishes, for example apply custom color transforms and whatnot. Just code them in fragment shader and fire away!


## INSTALLATION
Just run the application executable. For loading files, File->Open or drag-drop to application window. Or add player as default application for opening braw files, this will launch it when opening .braw file.


## USAGE
Play the file and fiddle with settings. If knob is active, values can be changed by scrolling mouse button or arrow keys. To reset a setting, right click on slider.

- Press M for metadata overlay, it displays all meta read from file, including dynamically changing meta.
- Press D for debug data, it doesn't show much useful though.
- Press G for gamut plot. Not useful, also wrong with any other gamut-gamma than linear XYZ.
- Press V to toggle through viewer modes: full data, minimal player, video only. Has some layout issues.
- Press T to show timecode in playhead TC knob. Might be wrong, check metadata, there is correct tc for frame.

Use mouse scroll button for zooming and left button for panning the viewer. Double-click left button to reset view.

Viewer gain and gamma affect the final decoded image. Decoding precision affects decoder output data format, it is clear that decoder outputs unclipped data in float mode and clipped data in int mode.
Viewer LUT combobox is populated with shaders from shaders/ dir, shader is applied to image in gpu during rendering stage.

## CACHING
New caching mechanism is added since version 0.5.0. It is a work in progress but should help with playback problems in weaker machines. Cached frames are marked with blue line in timeline slider.

Frames are cached in the background into RAM and caching setting in UI controls the auto pre-cache behavior. If always on, pre-cache tries to fill cache in forward-looking manner until full and caching also happens during playback. When set to cache only when paused, caching stops when playback is ongoing, this makes playback less prone to lagging. Pre-cache set to off means cache does not fill automatically but when playing or scrubbing, cache is still filled with decoded frames.

Since caching is currently experimental, it is capped to 100 frames for float storage (but expands 2-4x depending on decode data type). For overriding this capped value set command line argument -cache to some other value, for example -cache 200

## COMMAND LINE ARGUMENTS
Since version 0.5.0 command line arguments allow launching player with specified file and also trim braw files.

Arguments are as follows:
- -f <file>		specify .braw file to open
- -i <inpoint>		specify in point (used for trimming and also sets it in gui)
- -o <outpoint>		specify out point (used for trimming and also sets it in gui)
- -trim <newfile>		specify name of new file and launch trim job without gui. Without this parameter, GUI is launched as usual.
- -cache <numframes> 	override default cache size

In and out points are clipped to video frame range, so if out is set as very big number it is possible to trim just from start for example. This will be improved in the future to support negative values for trimming relative to start and end.

### Examples:
Open .braw file

	brawplayer.exe -f /path-to-folder/myvideo.braw

Open .braw file	with io	points set in GUI

	brawplayer.exe -f /path-to-folder/myvideo.braw -i 10 -o 100

Trim .braw file	with specified io points to new .braw file

	brawplayer.exe -f /path-to-folder/myvideo.braw -trim /path-to-folder/myvideo_trimmed.braw -i 10 -o 100


## WHAT WORKS
It opens and plays braw files :D Most of development settings should work, also dynamic data from sidecar files (change "Update from sidecar" to "on every frame" for dynamic data). Dynamic data means that exposure, tint etc (not all, but some properties) can be animated in sidecar. There is little documentation about this feature, bang on BMD-s door for better docs.


## WHAT DOES NOT WORK
No audio again. No GPU decode yet. Might crash randomly. Might do something else wrong. Export of sidecar do not work yet, I have to figure out how to properly submit metadata to clip for this.


## KNOWN BUGS
Braw trimming with timecode based IO points can result in cut being in wrong place. Cause is wobbly timecode to frame conversion, will be fixed. Use frame numbers in the meantime or set IO in GUI based on timecode display (toggle with T key).

Player can print out _"SetProcessDpiAwareness...  failed: COM error ..."_ message to console, it is not an actual issue.

## OTHER STUFF
For customization of UI appearance, fiddle with stylesheet.txt file in application dir.

Viewer surface appearance is fully customizable through vertex and fragment shaders in shaders/ subdir. One can use them for implementing custom LUTs etc.
See example shaders in their folder. Each shader file with .frag ending can be loaded in LUT combobox (on runtime) and is applied to viewer. passthrough shader is the default one and does nothing.

Example of how to use dynamically changing development settings in sidecar file. To get it update in player, set "Update from sidecar" combo (in player settings group) to "On every frame":
```
{
    "tone_curve_contrast": 1.500000,
	"tone_curve_saturation": 1.600000,
	"tone_curve_midpoint": 0.300000,
	"tone_curve_highlights": 0.650000,
	"tone_curve_shadows": 2.000000,
    "tone_curve_video_black_level": 1,
	"viewing_gamma": "Blackmagic Design Custom",
	"viewing_gamut": "Blackmagic Design",
	"viewing_bmdgen": 4,
	"exposure": {
        "12:01:40:04": 0.400000,
        "12:01:41:04": 0.600000,
        "12:01:42:04": 0.800000,
        "12:01:43:04": 1.000000,
        "12:01:44:04": 1.200000
	},
	"white_balance_kelvin": {
		"12:01:40:04": 4000,
        "12:01:41:04": 4500,
        "12:01:42:04": 5000,
        "12:01:43:04": 5500
	},
	"white_balance_tint": {
		"12:01:40:04": -20
	},
	"iso": {
		"12:01:40:04": 800
	}
}
```

### VIEWER SHADER UNIFORM LIST
```
uniform sampler2D tex;  	// video texture
uniform sampler2D tex1; 	// image texture1
uniform sampler2D tex2;		// image texture2
uniform sampler2D tex3;		// image texture3
uniform sampler2D tex4;		// image texture4
uniform vec2 resolution;	// video resolution
uniform vec2 decoderesolution;	// video decoding resolution
uniform float exposure;		// viewer exposure
uniform float gamma;		// viewer gamma
uniform int bitdepth;		// buffer bit depth value (combobox index)
uniform int lut;		// lut index value (combobox index)
uniform float lutweight;	// lut weight from UI
in vec2 fragTexCoord;		// video texture coordinate values in 0.0-1.0 range
```