#version 150 core
uniform sampler2D tex;
uniform vec2 resolution;
uniform vec2 decoderesolution;
uniform float exposure;
uniform float gamma;
uniform int bitdepth;
uniform int lut;
uniform float lutweight;
in vec2 fragTexCoord;

vec4 applyViewerGrade(vec4 basecolor)
{
    vec4 color = basecolor;
    
    // General exposure and gamma adjustments
    color = color * pow(2, exposure);
    color.r = pow(color.r, gamma);
    color.g = pow(color.g, gamma);
    color.b = pow(color.b, gamma);
    
    return color;
}

void main(void)
{
    vec4 color = texture2D(tex, fragTexCoord);
    vec4 finalcolor = color;
    
    // Number of steps is granularity of histogram in height and also value range
    float numSteps = decoderesolution.y;
    float stepSize = 1.0 / numSteps;
    for (int y = 0; y < numSteps; y++)
    {
        vec2 uv = vec2(fragTexCoord.x, stepSize * y);
        vec4 sc = texture2D(tex, uv);
        
        // Box variable sets the current rank we look for
        float box = floor((1.0 - fragTexCoord.y) * numSteps) / numSteps;
        
        // If pixel value happens to be in this rank, set output color for that channel to 1.0
        if ((floor(sc.r * numSteps) / numSteps) == box)
            finalcolor.r = 1.0;
        if ((floor(sc.g * numSteps) / numSteps) == box)
            finalcolor.g = 1.0;
        if ((floor(sc.b * numSteps) / numSteps) == box)
            finalcolor.b = 1.0;
    }

    // Apply weighting and viewer grade
    gl_FragColor = mix(applyViewerGrade(color), applyViewerGrade(finalcolor), lutweight);
};