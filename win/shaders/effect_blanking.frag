#version 150 core
uniform sampler2D tex;
uniform vec2 resolution;
uniform float exposure;
uniform float gamma;
uniform int bitdepth;
uniform int lut;
uniform float lutweight;
in vec2 fragTexCoord;

vec4 applyViewerGrade(vec4 basecolor)
{
    vec4 color = basecolor;
    
    // General exposure and gamma adjustments
    color = color * pow(2, exposure);
    color.r = pow(color.r, gamma);
    color.g = pow(color.g, gamma);
    color.b = pow(color.b, gamma);
    
    return color;
}

void main(void)
{
    // Sample image from input texture
    vec4 color = texture2D(tex, fragTexCoord);

    vec2 uvcoords = vec2(fragTexCoord.x, fragTexCoord.y);
    float aspect = resolution.x / resolution.y;
    float cropaspect = 2.35;
    float blanking = aspect / cropaspect;
    float cropratio = (1 - blanking) / 2;
    
    vec4 finalcolor = color;
    if (uvcoords.y < cropratio)
        finalcolor = vec4(0.0);
        
    if (uvcoords.y > 1 - cropratio)
        finalcolor = vec4(0.0);

    // Apply weighting and viewer grade
    gl_FragColor = mix(applyViewerGrade(color), applyViewerGrade(finalcolor), lutweight);
};