#version 150 core
uniform sampler2D tex;
uniform vec2 resolution;
uniform float exposure;
uniform float gamma;
uniform int bitdepth;
uniform int lut;
uniform float lutweight;
in vec2 fragTexCoord;

vec4 applyViewerGrade(vec4 basecolor)
{
    vec4 color = basecolor;
    
    // General exposure and gamma adjustments
    color = color * pow(2, exposure);
    color.r = pow(color.r, gamma);
    color.g = pow(color.g, gamma);
    color.b = pow(color.b, gamma);
    
    return color;
}

void main(void)
{
    vec4 color = texture2D(tex, fragTexCoord);

    // YCbCr conversion
    vec4 finalcolor = color;
    
    finalcolor.r = 0.299 * color.r + 0.587 * color.g + 0.114 * color.b;
    finalcolor.g = -0.169 * color.r - 0.331 * color.g + 0.499 * color.b + 0.5;
    finalcolor.b = 0.499 * color.r - 0.418 * color.g - 0.0813 * color.b + 0.5;

    // This example demonstated the Cb channel of YCbCr data
    finalcolor = vec4(vec3(finalcolor.g), 1.0);
    
    // Apply weighting and viewer grade
    gl_FragColor = mix(applyViewerGrade(color), applyViewerGrade(finalcolor), lutweight);
};