#version 150 core
uniform sampler2D tex;
uniform sampler2D tex1;
uniform sampler2D tex2;
uniform sampler2D tex3;
uniform sampler2D tex4;
uniform vec2 resolution;
uniform float exposure;
uniform float gamma;
uniform int bitdepth;
uniform int lut;
uniform float lutweight;
in vec2 fragTexCoord;

vec4 applyViewerGrade(vec4 basecolor)
{
    vec4 color = basecolor;
    
    // General exposure and gamma adjustments
    color = color * pow(2, exposure);
    color.r = pow(color.r, gamma);
    color.g = pow(color.g, gamma);
    color.b = pow(color.b, gamma);
    
    return color;
}

void main(void)
{
    vec4 color = texture2D(tex, fragTexCoord);
    vec4 texture4 = texture2D(tex4, fragTexCoord);
    
    // This is just a logo overlay
    
    // Premultiplication of RGB values
    vec4 premulted = vec4(vec3(texture4) * texture4.a, texture4.a);
    
    // Merge operation
    vec4 finalcolor = color * (1.0 - texture4.a) + premulted;
    
    // Apply grade
    gl_FragColor = gl_FragColor = mix(applyViewerGrade(color), applyViewerGrade(finalcolor), lutweight);
};