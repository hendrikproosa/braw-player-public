#version 150 core
uniform sampler2D tex;
uniform vec2 resolution;
uniform float exposure;
uniform float gamma;
uniform int bitdepth;
uniform int lut;
uniform float lutweight;
in vec2 fragTexCoord;

vec4 applyViewerGrade(vec4 basecolor)
{
    vec4 color = basecolor;
    
    // General exposure and gamma adjustments
    color = color * pow(2, exposure);
    color.r = pow(color.r, gamma);
    color.g = pow(color.g, gamma);
    color.b = pow(color.b, gamma);
    
    return color;
}

void main(void)
{
    vec4 color = texture2D(tex, fragTexCoord);

    // bt1886 (by default gamma 2.4) transfer
    float cLin = 0.0;
    float cSlope = 0.0;
    float cGain = 1.0;
    float cOffset = 0.0;
    float cPow = 1.0/2.4;
    vec4 finalcolor = color;
    if (finalcolor.r < cLin)
    {
        finalcolor = finalcolor * cSlope;
    } else {
        finalcolor.r = pow(finalcolor.r, cPow) * cGain - cOffset;
        finalcolor.g = pow(finalcolor.g, cPow) * cGain - cOffset;
        finalcolor.b = pow(finalcolor.b, cPow) * cGain - cOffset;
    }

    gl_FragColor = mix(applyViewerGrade(color), applyViewerGrade(finalcolor), lutweight);
};