#version 150 core
uniform sampler2D tex;
uniform vec2 resolution;
uniform float exposure;
uniform float gamma;
uniform int bitdepth;
uniform int lut;
uniform float lutweight;
in vec2 fragTexCoord;

vec4 applyViewerGrade(vec4 basecolor)
{
    vec4 color = basecolor;
    
    // General exposure and gamma adjustments
    color = color * pow(2, exposure);
    color.r = pow(color.r, gamma);
    color.g = pow(color.g, gamma);
    color.b = pow(color.b, gamma);
    
    return color;
}

void main(void)
{
    vec4 color = texture2D(tex, fragTexCoord);

    vec2 texOffset = vec2(0.01);
    vec2 tc0 = fragTexCoord.st + vec2(-texOffset.s, -texOffset.t);
    vec2 tc1 = fragTexCoord.st + vec2(         0.0, -texOffset.t);
    vec2 tc2 = fragTexCoord.st + vec2(+texOffset.s, -texOffset.t);
    vec2 tc3 = fragTexCoord.st + vec2(-texOffset.s,          0.0);
    vec2 tc4 = fragTexCoord.st + vec2(         0.0,          0.0);
    vec2 tc5 = fragTexCoord.st + vec2(+texOffset.s,          0.0);
    vec2 tc6 = fragTexCoord.st + vec2(-texOffset.s, +texOffset.t);
    vec2 tc7 = fragTexCoord.st + vec2(         0.0, +texOffset.t);
    vec2 tc8 = fragTexCoord.st + vec2(+texOffset.s, +texOffset.t);

    vec4 col0 = texture2D(tex, tc0);
    vec4 col1 = texture2D(tex, tc1);
    vec4 col2 = texture2D(tex, tc2);
    vec4 col3 = texture2D(tex, tc3);
    vec4 col4 = texture2D(tex, tc4);
    vec4 col5 = texture2D(tex, tc5);
    vec4 col6 = texture2D(tex, tc6);
    vec4 col7 = texture2D(tex, tc7);
    vec4 col8 = texture2D(tex, tc8);

    vec4 finalcolor = (1.0 * col0 + 2.0 * col1 + 1.0 * col2 + 
            2.0 * col3 + 4.0 * col4 + 2.0 * col5 +
            1.0 * col6 + 2.0 * col7 + 1.0 * col8) / 16.0; 
    
    // Apply weighting and viewer grade
    gl_FragColor = mix(applyViewerGrade(color), applyViewerGrade(finalcolor), lutweight);
};