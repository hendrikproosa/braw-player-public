#version 150 core
uniform sampler2D tex;
uniform sampler2D tex1;
uniform sampler2D tex2;
uniform sampler2D tex3;
uniform sampler2D tex4;
uniform vec2 resolution;
uniform float exposure;
uniform float gamma;
uniform int bitdepth;
uniform int lut;
uniform float lutweight;
in vec2 fragTexCoord;

vec4 applyViewerGrade(vec4 basecolor)
{
    vec4 color = basecolor;
    
    // General exposure and gamma adjustments
    color = color * pow(2, exposure);
    color.r = pow(color.r, gamma);
    color.g = pow(color.g, gamma);
    color.b = pow(color.b, gamma);
    
    return color;
}

void main(void)
{
    vec4 color = texture2D(tex, fragTexCoord);
    vec4 texture3 = texture2D(tex3, fragTexCoord);
    
    // This is an example of video distortion based on input image
    // For this, we use texture input 3 as uv coordinates for lookup into original video
    vec4 finalcolor = texture2D(tex, vec2(texture3.r, 1.0 - texture3.g));
    
    // Apply grade
    gl_FragColor = gl_FragColor = mix(applyViewerGrade(color), applyViewerGrade(finalcolor), lutweight);
};