#version 150 core
uniform sampler2D tex;
uniform vec2 resolution;
uniform float exposure;
uniform float gamma;
uniform int bitdepth;
uniform int lut;
uniform float lutweight;
in vec2 fragTexCoord;

const float PI = 3.1415926535;

vec4 applyViewerGrade(vec4 basecolor)
{
    vec4 color = basecolor;
    
    // General exposure and gamma adjustments
    color = color * pow(2, exposure);
    color.r = pow(color.r, gamma);
    color.g = pow(color.g, gamma);
    color.b = pow(color.b, gamma);
    
    return color;
}

vec2 Distort(vec2 p)
{
    float theta  = atan(p.y, p.x);
    float radius = length(p);
    radius = pow(radius, 1.5 - lutweight);
    p.x = radius * cos(theta);
    p.y = radius * sin(theta);
    return 0.5 * (p + 1.0);
}

void main(void)
{
    vec2 xy = 2.0 * fragTexCoord - 1.0;
    vec2 uv;
    float d = length(xy);
    if (d < 1.0)
    {
        uv = Distort(xy);
    }
    else
    {
        uv = fragTexCoord;
    }
    vec4 color = texture2D(tex, uv);
    
    // Apply grade
    gl_FragColor = applyViewerGrade(color);
};