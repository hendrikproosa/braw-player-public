#version 150 core
uniform sampler2D tex;
uniform vec2 resolution;
uniform float exposure;
uniform float gamma;
uniform int bitdepth;
uniform int lut;
uniform float lutweight;
in vec2 fragTexCoord;

vec4 applyViewerGrade(vec4 basecolor)
{
    vec4 color = basecolor;
    
    // General exposure and gamma adjustments
    color = color * pow(2, exposure);
    color.r = pow(color.r, gamma);
    color.g = pow(color.g, gamma);
    color.b = pow(color.b, gamma);
    
    return color;
}

void main(void)
{
    // Sample from base coords
    vec4 color = texture2D(tex, fragTexCoord);
    
    // Modify color with vignette
    float vignette = 2 * pow(pow(fragTexCoord.x, 2) * pow(fragTexCoord.y, 2), 0.5);
    vignette = pow(vignette, gamma);
    vec4 finalcolor = color * vignette;
    
    // Apply weighting and viewer grade
    gl_FragColor = mix(applyViewerGrade(color), applyViewerGrade(finalcolor), lutweight);
};