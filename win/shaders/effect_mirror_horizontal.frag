#version 150 core
uniform sampler2D tex;
uniform vec2 resolution;
uniform float exposure;
uniform float gamma;
uniform int bitdepth;
uniform int lut;
uniform float lutweight;
in vec2 fragTexCoord;

vec4 applyViewerGrade(vec4 basecolor)
{
    vec4 color = basecolor;
    
    // General exposure and gamma adjustments
    color = color * pow(2, exposure);
    color.r = pow(color.r, gamma);
    color.g = pow(color.g, gamma);
    color.b = pow(color.b, gamma);
    
    return color;
}

void main(void)
{
    // Sample from base coords
    vec4 color = texture2D(tex, fragTexCoord);
    
    // Sample from mirrored uv coords
    vec2 mirroruv = vec2(1.0 - fragTexCoord.x, fragTexCoord.y);
    vec4 finalcolor = texture2D(tex, mirroruv);
    
    // Apply weighting and viewer grade
    gl_FragColor = mix(applyViewerGrade(color), applyViewerGrade(finalcolor), lutweight);
};